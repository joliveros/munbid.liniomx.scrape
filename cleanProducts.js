var request = require('request');
var cheerio = require('cheerio');
var queue = require('queue');
var url = require('url');
var _u = require('underscore.string');
require('./stringUtilities')
var linioProduct = require('../db.munbid_liniomx/productSchema')
var baseUrl = "http://www.linio.com.mx";

var q = new queue({
    timeout: 100000,
    concurrency: 100
});

// listen for events

q.on('processed', function(job) {
    console.log('job finished processing:', job.toString().replace(/\n/g, ''));
});

q.on('drain', function() {
    console.log('all done:');
});

q.on('timeout', function(job, next) {
    console.log('job timed out:', job.toString().replace(/\n/g, ''));
    next();
});
var init = function() {
    queueGetBatchProducts(0)
}
var count=0;
var GetBatchProducts = function(count, cb) {
    linioProduct.find().skip(count).limit(10).exec(function(err, result) {
        if (err) return handleError(err)
        // console.log(result)
        var l = result.length
        count = count + l;
        if (l > 0) {
            for (var i = 0; i < result.length; i++) {
                var product = result[i]
                product['recommendedItemsUrl'] = undefined;
                product['recommended'] = undefined;
                product.save(function(err, p) {
                    if (err)
                        throw err
                    count++
                    console.log(count)
                    cb()
                })
            }
            queueGetBatchProducts(count)
        }
        else
            cb()
    })
}
var queueGetBatchProducts = function(count) {
    q.push(function(cb) {
        GetBatchProducts(count, cb)
    })
}
init()
