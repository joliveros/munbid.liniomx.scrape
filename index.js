var request= require('request'),
_=require('underscore')
var cheerio= require('cheerio');
var queue= require('queue');
var url= require('url')
var MunbidDb= require('munbid.db')
var baseUrl= "http://www.linio.com.mx";
var categoryUrls=['http://www.linio.com.mx/computadoras/',
    'http://www.linio.com.mx/camaras/',
    'http://www.linio.com.mx/tv-audio-y-video/televisiones/',
    'http://www.linio.com.mx/tv-audio-y-video/audio/',
    'http://www.linio.com.mx/tv-audio-y-video/video/',
    'http://www.linio.com.mx/tv-audio-y-video/instrumentos-musicales/',
    'http://www.linio.com.mx/celulares-telefonia-y-gps/',
    'http://www.linio.com.mx/videojuegos/',
    'http://www.linio.com.mx/electrodomesticos/',
    'http://www.linio.com.mx/moda/relojes/',
    'http://www.linio.com.mx/moda/bolsas-y-carteras/',
    'http://www.linio.com.mx/moda/lentes/',
    'http://www.linio.com.mx/moda/joyeria/',
    'http://www.linio.com.mx/salud-y-cuidado-personal/perfumes/'
],
categories=[]
require('./stringUtilities.js')
// Import Underscore.string to separate object, because there are conflict functions (include, reverse, contains)
_.str = require('underscore.string');

// Mix in non-conflict functions to Underscore namespace if you want
_.mixin(_.str.exports());

// All functions, include conflict, will be available through _.str object
_.str.include('Underscore.string', 'string'); // => true
//get html

var q = new queue({
    timeout: 100000,
    concurrency: 10
});

// listen for events

q.on('processed', function(job) {
    console.log('job finished processing:', job.toString().replace(/\n/g, ''));
});

q.on('drain', function() {
    console.log('all done:');
});

q.on('timeout', function(job, next) {
    console.log('job timed out:', job.toString().replace(/\n/g, ''));
    next();
});
var init = function() {
    for (var i = 0; i < categoryUrls.length; i++) {
        var u = categoryUrls[i];
        queueCategoryPage({
            page: 1,
            baseUrl: u
        });
    }
}
var queueCategoryPage = function(urlInfo) {
    q.push(function(cb) {
        scrapeProducts(urlInfo, cb);
    })
}

init()
// var scrapeCategory = function(categoryUrl) {
//     q.push(function(cb) {
//         request(categoryUrl, function(err, resp, body) {
//             if (err) {
//                 throw err;
//             }
//             var $ = cheerio.load(body);
//             // var t= $('.paging-nav .catalog-pages .ui-listitem').last().html();
//             var t = $('.paging-nav ul li a').last().attr('href');
//             // console.log(categoryUrl)
//             console.log($('.paging-nav').html())
//             var params = url.parse(t, true);
//             var pages = params.query.page;
//             do {
//                 queueCategoryPage({
//                     page: pages,
//                     baseUrl: categoryUrl,
//                     lastPageProduct: '',
//                 });
//                 pages -= 1;
//             } while (pages > 0);
//             //callback when done
//             cb();
//         });
//     });

// }

var scrapeProducts = function(urlInfo, cb) {
    urlInfo.url = urlInfo.baseUrl + '?page=' + urlInfo.page;

    request(urlInfo.url, function(err, resp, body) {
        if (err) {
            throw err;
        }
        $ = cheerio.load(body);
        var last = $('.product-item a').first().attr('href')
        if (last != urlInfo.lastPageProduct) {
            urlInfo.page = urlInfo.page + 1;
            urlInfo.lastPageProduct = last
            queueCategoryPage(urlInfo)
        }

        $('.product-item a').each(function(i, elem) {
            var href = $(this).attr('href');
            var productUrl = baseUrl + href;
            var u={}
            u.productUrl = productUrl
            u.url=urlInfo.url
            u.page=urlInfo.page
            u.baseUrl=urlInfo.baseUrl
            var id=_(u.productUrl).trim(baseUrl+'/')
            id=_(id).strLeft('.html')
            queueProductScrape(u)
            // MunbidDb.Product.findById(id).select('_id').exec(function(err, product) {
            //     if (err) throw err
            //     if (!product)
            //         {
            //             // console.log(u);
            //             queueProductScrape(u)
            //         }
            //     else
            //         console.log('already exists: ' + id)
            //     cb();
            // })
            cb()
        });
        //callback when done

    });
}
var queueProductScrape = function(urlInfo) {
    q.push(function(cb) {
        getHtml(urlInfo, cb);
    })

}
var getHtml = function(urlInfo, cb) {
    request(urlInfo.productUrl, function(err, resp, body) {
        if (err)
            console.log(err);
        var $ = cheerio.load(body);
        var id=_(urlInfo.productUrl).trim(baseUrl+'/')
        id=_(id).strLeft('.html')
        var category=_(urlInfo.baseUrl).strRight(baseUrl)
        product = {}
        product.c=category
        product.t = $('#title-product-detail h1').text();
    if(product.c!=null){
        var catUrl=_(product.c.replace(baseUrl, '')).trim('/')
        var cats=catUrl.split('/')
    }
    product.r = []
    var prices = $('#product-prices');
    product.p = prices.find('#product-special-price').text().PriceStringToNumber();
    var images = [];
    $('#product-more-images .swiper-wrapper li a').each(function(i, elem) {
        images.push(this.attr('data-image-big'));
    });
    product.i = images;
    var variations = [];
    $('.prd-option-collection').each(function(i, elem) {
        variations.push(this.text().trim());
    })
    $('#product-table-description tr').each(function(i, elem) {
        var row = $($(this).children());
        var prop = row.first().text();
        prop = prop.toLowerCase();
        if(prop=='sku')
        product[prop] = row.last().text();
    })
    var productReviews = $('.product-review');
    if (productReviews.length > 0) {
        productReviews.each(function(i, elem) {
            var revHtml = $(this[i])
            var productReview = {};
            productReview.Author = revHtml.find('.product-review-author em').text();
            var dateHtml = revHtml.find('.product-review-author').text().match(/[0-9\/]+/);
            if (dateHtml != null)
                productReview.Date = _(dateHtml[0]).trim();
            productReview.Title = revHtml.find('.product-review-title').text().trim();
            productReview.Comment = revHtml.find('.product-review-comment').text().trim();
            var ranking = revHtml.find('.product-review-rating-stars');
            var price = $(ranking.toArray()[0]).attr('style');
            if (price != null)
                productReview.Price = price.match(/[0-9]+/)[0]
            var appearance = $(ranking.toArray()[1]).attr('style');
            if (appearance != null)
                productReview.Appearance = appearance.match(/[0-9]+/)[0]
            var quality = $(ranking.toArray()[2]).attr('style');
            if (quality != null)
                productReview.Quality = quality.match(/[0-9]+/)[0]
            product.r.push(productReview);
        })
    }
    $('#product-table-description').remove();
    product.d = $('#product-description').html();
    product.v = variations;
    MunbidDb.Product.findOneAndUpdate({_id: id},product, {upsert: true, select:'_id'}, function(err, p) {
            if (err)
                throw err
            console.log(product)
            cb()
        })
    })
}
function updateProduct(product, cb){
    // product.id=id
    console.log(product);
    cb()
        // MunbidDb.Product.findOneAndUpdate({_id: id},product, {upsert: true}, function(err, p) {
        //     if (err)
        //         throw err
        //     console.log(p)
        //     cb()
        // })
}
