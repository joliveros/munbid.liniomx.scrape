var _u = require('underscore.string');
String.prototype.PriceStringToNumber = function() {
    var Price = this.replace(/[\s+\,\$]/g, '');
    return _u.toNumber(Price);
};
