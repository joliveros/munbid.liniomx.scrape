var request = require('request');
var cheerio = require('cheerio');
var queue = require('queue');
var url = require('url');
var linioProduct = require('../db.munbid_liniomx/productSchema')
var _u = require('underscore.string');
var baseUrl = "http://www.linio.com.mx";
var categoryUrls = ['http://www.linio.com.mx/computadoras/',
    'http://www.linio.com.mx/camaras/',
    'http://www.linio.com.mx/tv-audio-y-video/televisiones/',
    'http://www.linio.com.mx/tv-audio-y-video/audio/',
    'http://www.linio.com.mx/tv-audio-y-video/video/',
    'http://www.linio.com.mx/tv-audio-y-video/instrumentos-musicales/',
    'http://www.linio.com.mx/celulares-telefonia-y-gps/',
    'http://www.linio.com.mx/videojuegos/',
    'http://www.linio.com.mx/electrodomesticos/',
    'http://www.linio.com.mx/moda/relojes/',
    'http://www.linio.com.mx/moda/bolsas-y-carteras/',
    'http://www.linio.com.mx/moda/lentes/',
    'http://www.linio.com.mx/moda/joyeria/',
    'http://www.linio.com.mx/salud-y-cuidado-personal/perfumes/'
];

var q = new queue({
    timeout: 100000,
    concurrency: 10
});

// listen for events

q.on('processed', function(job) {
    console.log('job finished processing:', job.toString().replace(/\n/g, ''));
});

q.on('drain', function() {
    console.log('all done:');
});

q.on('timeout', function(job, next) {
    console.log('job timed out:', job.toString().replace(/\n/g, ''));
    next();
});
var init = function() {
    for (var i = 0; i < categoryUrls.length; i++) {
        var u = categoryUrls[i];
        queueCategoryPage({
            page: 1,
            baseUrl: u
        });
    }
}
var scrapeCategory = function(categoryUrl) {
    q.push(function(cb) {
        request(categoryUrl, function(err, resp, body) {
            if (err) {
                throw err;
            }
            var $ = cheerio.load(body);
            // var t= $('.paging-nav .catalog-pages .ui-listitem').last().html();
            var t = $('.paging-nav ul li a').last().attr('href');
            // console.log(categoryUrl)
            console.log($('.paging-nav').html())
            var params = url.parse(t, true);
            var pages = params.query.page;
            do {
                queueCategoryPage({
                    page: pages,
                    baseUrl: categoryUrl,
                    lastPageProduct: '',
                });
                pages -= 1;
            } while (pages > 0);
            //callback when done
            cb();
        });
    });

}
var queueCategoryPage = function(urlInfo) {
    q.push(function(cb) {
        scrapeProducts(urlInfo, cb);
    })
}
var scrapeProducts = function(urlInfo, cb) {
    urlInfo.url = urlInfo.baseUrl + '?page=' + urlInfo.page;

    request(urlInfo.url, function(err, resp, body) {
        if (err) {
            throw err;
        }
        $ = cheerio.load(body);
        var last = $('.product-item a').first().attr('href')
        if (last != urlInfo.lastPageProduct) {
            urlInfo.page = urlInfo.page + 1;
            urlInfo.lastPageProduct = last
            queueCategoryPage(urlInfo)
        }

        $('.product-item a').each(function(i, elem) {
            var href = $(this).attr('href');
            var productUrl = baseUrl + href;
            var u={}
            u.productUrl = productUrl
            u.url=urlInfo.url
            u.page=urlInfo.page
            u.baseUrl=urlInfo.baseUrl
            linioProduct.findOne({
                'url': u.productUrl
            }, function(err, product) {
                if (err) return handleError(err);
                if (product == null)
                    {
                        queueProductScrape(u)
                    }
                else
                    console.log('already exists: ' + u.productUrl)
                cb();
            })
        });
        //callback when done
        
    });
}
var queueProductScrape = function(urlInfo) {
    q.push(function(cb) {
        saveProductHtml(urlInfo, cb);
    })

}
var saveProductHtml = function(urlInfo, cb) {
    request(urlInfo.productUrl, function(err, resp, body) {
        if (err)
            console.log(err);
        product = new linioProduct()
        product.url = urlInfo.productUrl
        product.categoryUrl=urlInfo.baseUrl
        product.html = body;
        product.save(function(err, p) {
            if (err)
                throw err
            console.log(p.url)
            cb()
        })
    })
}
init()
