var request = require('request');
var cheerio = require('cheerio');
var queue = require('queue');
var url = require('url');
var _u = require('underscore.string');
require('./stringUtilities')
var linioProduct = require('../db.munbid_liniomx/productSchema')
var baseUrl = "http://www.linio.com.mx";

var q = new queue({
    timeout: 100000,
    concurrency: 100
});

// listen for events

q.on('processed', function(job) {
    console.log('job finished processing:', job.toString().replace(/\n/g, ''));
});

q.on('drain', function() {
    console.log('all done:');
});

q.on('timeout', function(job, next) {
    console.log('job timed out:', job.toString().replace(/\n/g, ''));
    next();
});
var mongo = require('mongodb');
var Grid = require('gridfs-stream');

// create or use an existing mongodb-native db instance.
// for this example we'll just create one:
var db = new mongo.Db('munbid_liniomx', new mongo.Server('localhost', 27017), {
    safe: true
});

// make sure the db instance is open before passing into `Grid`
var gfs = Grid(db, mongo);
db.open(function(err) {
    if (err) return handleError(err);

    init()
    // all set!
})
var init = function() {
    queueGetBatchImageUrls(0)
}
var getBatchImageUrls = function(count, cb) {
    linioProduct.find().select('images').skip(count).limit(10).exec(function(err, result) {
        if (err) return handleError(err)
        // console.log(result)
        var l = result.length
        count = count + l;
        if (l > 0) {
            for (var i = 0; i < result.length; i++) {
                var imgs = result[i].images
                if (imgs.length > 0) {
                    for (var f = 0; f < imgs.length; f++) {
                        queueImageDownload(imgs[f])
                    }
                }
            }
            queueGetBatchImageUrls(count)
        }
        cb()
    })
}
var counter = 0;
var queueImageDownload = function(imageUrl) {
    q.push(function(cb) {
        imageDownload(imageUrl, cb)
    })
}

var imageDownload = function(imageUrl, cb) {
    var filename = imageUrl.match(/[0-9a-zA-z\.\-]+.jpg/)[0]
    gfs.files.find({
        filename: filename
    }).toArray(function(err, files) {
        if (err) throw err;
        if (files.length == 0) {
            var writestream = gfs.createWriteStream({
                filename: filename
            });

            // console.log(writestream)
            writestream.on('close', function(file) {
                // do something with `file`
                console.log(file.filename);
                cb()
            });
            request(imageUrl).pipe(writestream)
        } else {
            console.log('file exists: ' + filename)
            cb()
        }

    })

}
var queueGetBatchImageUrls = function(count) {
    q.push(function(cb) {
        getBatchImageUrls(count, cb)
    })
}
// init()
