var request = require('request');
var cheerio = require('cheerio');
var queue = require('queue');
var url = require('url');
var _u = require('underscore.string');
require('./stringUtilities')
var linioProduct = require('../db.munbid_liniomx/productSchema')
var baseUrl = "http://www.linio.com.mx";

var q = new queue({
    timeout: 100000,
    concurrency: 100
});

// listen for events

q.on('processed', function(job) {
    console.log('job finished processing:', job.toString().replace(/\n/g, ''));
});

q.on('drain', function() {
    console.log('all done:');
});

q.on('timeout', function(job, next) {
    console.log('job timed out:', job.toString().replace(/\n/g, ''));
    next();
});
var init = function() {
    queueGetBatchHtml(0)
}
var getBatchHtml = function(count, cb) {
    linioProduct.find().skip(count).limit(10).exec(function(err, result) {
        if (err) return handleError(err)
        var l = result.length
        // console.log(result)
        count = count + l;
        if (l > 0) {
            for (var i = 0; i < result.length; i++) {
                queueHtmlParse(result[i]);
            }
            queueGetBatchHtml(count)
        }
        cb()
    })
}
var queueHtmlParse = function(product) {
    q.push(function(cb) {
        parseHtml(product, cb)
    })
}

var parseHtml = function(product, cb) {
    $ = cheerio.load(product.html);
    product.title = $('#title-product-detail h1').text();
    console.log(product.categoryUrl)
    if(product.categoryUrl!=null){
        var catUrl=_u.trim(product.categoryUrl.replace(baseUrl, ''), '/')
        var cats=catUrl.split('/')
    }
    if (product.title != null)
        var keywords = product.title.split(' ')
    product.keywords = keywords
    product.Reviews = []
    var prices = $('#product-prices');
    product.marketPrice = prices.find('#product-market-price').text().PriceStringToNumber();
    product.specialPrice = prices.find('#product-special-price').text().PriceStringToNumber();
    var images = [];
    $('#product-more-images .swiper-wrapper li a').each(function(i, elem) {
        images.push(this.attr('data-image-big'));
    });
    product.images = images;
    var variations = [];
    $('.prd-option-collection').each(function(i, elem) {
        variations.push(this.text().trim());
    })
    $('#product-table-description tr').each(function(i, elem) {
        var row = $($(this).children());
        var prop = row.first().text();
        prop = _u.classify(prop);
        product[prop] = row.last().text();
    })
    var recRegExRes = $('#last-products-viewed').next().text().match(/url: "([a-zA-Z\/\?\=\&0-9\_]+)"/);
    if (recRegExRes != null)
        product.recommendedItemsUrl = recRegExRes[1];
    product.ProductReview = null;
    var productReviews = $('.product-review');
    if (productReviews.length > 0) {
        productReviews.each(function(i, elem) {
            var revHtml = $(this[i])
            var productReview = {};
            productReview.Author = revHtml.find('.product-review-author em').text();
            var dateHtml = revHtml.find('.product-review-author').text().match(/[0-9\/]+/);
            if (dateHtml != null)
                productReview.Date = _u.trim(dateHtml[0]);
            productReview.Title = revHtml.find('.product-review-title').text().trim();
            productReview.Comment = revHtml.find('.product-review-comment').text().trim();
            var ranking = revHtml.find('.product-review-rating-stars');
            var price = $(ranking.toArray()[0]).attr('style');
            if (price != null)
                productReview.Price = price.match(/[0-9]+/)[0]
            var appearance = $(ranking.toArray()[1]).attr('style');
            if (appearance != null)
                productReview.Appearance = appearance.match(/[0-9]+/)[0]
            var quality = $(ranking.toArray()[2]).attr('style');
            if (quality != null)
                productReview.Quality = quality.match(/[0-9]+/)[0]
            product.Reviews.push(productReview);
        })
    }
    $('#product-table-description').remove();
    product.description = $('#product-description').html();
    product.variations = variations;
    product.save(function(err, p) {
        if (err)
            throw err
    })


    cb();
}
var queueGetBatchHtml = function(count) {
    q.push(function(cb) {
        getBatchHtml(count, cb)
    })
}
init()
